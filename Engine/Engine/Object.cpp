#include "Core.h"
#include "Object.h"

Object::Object()
{
	UUID uid;
	CreateUUID(&uid);

	id = GUIDToSTRCODE(uid);
	guid = GUIDTostring(uid);
}

void Object::Initialize()
{
	initialized = true;
}
