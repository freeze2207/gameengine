#include "Core.h"
#include "GameObject.h"

#include <iostream>
#include <assert.h>

#include "Component.h"

/// <summary>
/// Create the GameObject
/// </summary>
GameObject::GameObject()
{
}

/// <summary>
/// Destroy the GameObject
/// </summary>
GameObject::~GameObject()
{
	for (auto component : components)
	{
		delete component;
	}
	components.clear();
}

/// <summary>
/// Initialize the GameObject
/// </summary>
/// <param name="json_game_object"></param>
void GameObject::initialize(json::JSON& json_game_object)
{
	assert(json_game_object.hasKey("Name"));
	name = json_game_object["Name"].ToString();

	if (json_game_object.hasKey("Components"))
	{
		auto json_components = json_game_object["Components"];
		for (auto& json_component : json_components.ArrayRange())	// Loop for components
		{
			Component* component = new Component();
			component->Load(json_component);
			component->Initialize();
			AddComponent(component);
		}
	}
}

/// <summary>
/// Updates the GameObject
/// </summary>
void GameObject::update()
{
	for (auto component : components)
	{
		component->update();
	}
}

/// <summary>
/// Add a new component to the game object
/// </summary>
/// <param name="component"></param>
void GameObject::AddComponent(Component* component)
{
	components.push_back(component);
}

/// <summary>
/// Remove a component from the game object
/// </summary>
/// <param name="component"></param>
void GameObject::RemoveComponent(Component* component)
{
	components.remove(component);
}
