#pragma once
#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

class Asset;

/// <summary>
/// Defines the AssetManager class
/// </summary>
class AssetManager
{
private:
	std::list<Asset*> assets;

public:
	void initialize();
	void update();

	void AddAsset(Asset* asset);
	void RemoveAsset(Asset* asset);

	DECLARE_SINGLETON(AssetManager)
};

#endif //ASSETMANAGER_H
