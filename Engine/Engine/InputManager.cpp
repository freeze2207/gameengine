#include "Core.h"
#include "InputManager.h"
#include <iostream>

IMPLEMENT_SINGLETON(InputManager)

/// <summary>
/// Initialize the InputManager
/// </summary>
void InputManager::initialize()
{
	std::cout << "InputManager initialized" << std::endl;
}

/// <summary>
/// Updates the InputManager
/// </summary>
void InputManager::update()
{
}
