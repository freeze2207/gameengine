#include "Core.h"
#include "GameEngine.h"

#include <iostream>
#include <chrono>
#include <fstream>
#include <assert.h>

#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"
#include "json.hpp"

IMPLEMENT_SINGLETON(GameEngine)

/// <summary>
/// Initialize the GameEngine
/// </summary>
void GameEngine::initialize()
{
	std::ifstream game_settings_stream("../Assets/GameSettings.json");
	std::string game_settings_str((std::istreambuf_iterator<char>(game_settings_stream)), std::istreambuf_iterator<char>());
	json::JSON game_settings_document = json::JSON::Load(game_settings_str);

	RenderSystem::Instance().initialize(game_settings_document);
	FileSystem::Instance().initialize();
	InputManager::Instance().initialize();
	AssetManager::Instance().initialize();

	assert(game_settings_document.hasKey("GameEngine"));
	json::JSON game_engine = game_settings_document["GameEngine"];

	assert(game_engine.hasKey("DefaultFile"));
	std::string default_file = game_engine["DefaultFile"].ToString();

	std::ifstream default_file_stream(default_file.c_str());
	std::string default_file_str((std::istreambuf_iterator<char>(default_file_stream)), std::istreambuf_iterator<char>());
	json::JSON default_file_document = json::JSON::Load(default_file_str);

	GameObjectManager::Instance().initialize(default_file_document);
}

/// <summary>
/// Updates the GameEngine
/// </summary>
void GameEngine::update()
{
	std::chrono::time_point<std::chrono::system_clock> time;
	std::chrono::duration<float> deltaTime(0);
	std::chrono::duration<float> totalTime(0);

	while (totalTime.count() < 1.0f)
	{
		time = std::chrono::system_clock::now();

		FileSystem::Instance().update();
		AssetManager::Instance().update();

		InputManager::Instance().update();

		GameObjectManager::Instance().update();

		RenderSystem::Instance().update();

		deltaTime = std::chrono::system_clock::now() - time;
		totalTime += deltaTime;
	}
}