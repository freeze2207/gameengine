#pragma once
#ifndef FILESYSTEM_H
#define FILESYSTEM_H

/// <summary>
/// Defines the FileSystem class
/// </summary>
class FileSystem
{
public:
	void initialize();
	void load(const std::string& file_name);
	void update();

	DECLARE_SINGLETON(FileSystem)
};

#endif //FILESYSTEM_H
