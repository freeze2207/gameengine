#include "Core.h"
#include "GameObjectManager.h"
#include "GameObject.h"

#include <iostream>
#include <assert.h>

IMPLEMENT_SINGLETON(GameObjectManager)

/// <summary>
/// Destroy the GameObjectManager
/// </summary>
GameObjectManager::~GameObjectManager()
{
	for (auto game_object : game_objects)
	{
		delete game_object;
	}
	game_objects.clear();
}

/// <summary>
/// Initialize the GameObjectManager
/// </summary>
/// <param name="document">The json document</param>
void GameObjectManager::initialize(json::JSON& document)
{
	std::cout << "GameObjectManager initialized" << std::endl;

	if (document.hasKey("GameObjects") == false) return;

	json::JSON json_game_objects = document["GameObjects"];
	for (auto& json_game_object : json_game_objects.ArrayRange())
	{
		assert(json_game_object.hasKey("className"));
		std::string class_name = json_game_object["className"].ToString();

		GameObject* game_object = new GameObject();
		game_object->initialize(json_game_object);
		addGameObject(game_object);
	}
}

/// <summary>
/// Updates the GameObjectManager
/// </summary>
void GameObjectManager::update()
{
	for (auto game_object : game_objects)
	{
		game_object->update();
	}
}

/// <summary>
/// Add a GameObject
/// </summary>
void GameObjectManager::addGameObject(GameObject* game_object)
{
	game_objects.push_back(game_object);
}

/// <summary>
/// Remove a GameObject
/// </summary>
void GameObjectManager::removeGameObject(GameObject* game_object)
{
	game_objects.remove(game_object);
}