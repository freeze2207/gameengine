#pragma once
#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include <list>
#include "json.hpp"

class GameObject;

/// <summary>
/// Defines the GameObjectManager class
/// </summary>
class GameObjectManager
{
private:
	std::list<GameObject*> game_objects;

public:
	void initialize(json::JSON& document);
	void update();
	void addGameObject(GameObject* game_object);
	void removeGameObject(GameObject* game_object);

public:
	static GameObjectManager& Instance()
	{
		if (_instance == nullptr)
		{
			_instance = new GameObjectManager();
		}
		return *_instance;
	}
private:
	static GameObjectManager* _instance;

	GameObjectManager() = default;
	~GameObjectManager();
	GameObjectManager(const GameObjectManager& other) = delete;
	GameObjectManager& operator= (const GameObjectManager& other) = delete;
};

#endif //GAMEOBJECTMANAGER_H