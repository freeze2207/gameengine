#pragma once
#ifndef GAMEENGINE_H
#define GAMEENGINE_H

class RenderSystem;
class FileSystem;
class InputManager;
class AssetManager;
class GameObjectManager;

/// <summary>
/// Defines the GameEngine class
/// </summary>
class GameEngine
{

public:
	void initialize();
	void update();

	DECLARE_SINGLETON(GameEngine)
};

#endif //GAMEENGINE_H
