#include "Core.h"
#include "Component.h"
#include <iostream>
#include <assert.h>

void Component::Initialize()
{
	Object::Initialize();
}

void Component::Load(json::JSON& json_component)
{
	assert(json_component.hasKey("id"));
	id = json_component["id"].ToInt();
}

/// <summary>
/// Updates the Component
/// </summary>
void Component::update()
{
}
