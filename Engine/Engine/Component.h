#pragma once
#ifndef COMPONENT_H
#define COMPONENT_H

#include "Object.h"
#include "json.hpp"

/// <summary>
/// Defines the Component class
/// </summary>
class Component : public Object
{
private:
	int id = -1;

public:
	~Component() {};

	void Initialize() override;
	void Load(json::JSON& json_component);
	virtual void update();
};

#endif //COMPONENT_H