#pragma once
#ifndef SPRITE_H
#define SPRITE_H

#include "Component.h"
#include "IRenderable.h"

class Sprite : public Component, IRenderable
{
protected:
	void Render() override;
};

#endif // !SPRITE_H
