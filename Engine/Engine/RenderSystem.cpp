#include "Core.h"
#include "RenderSystem.h"
#include "IRenderable.h"

IMPLEMENT_SINGLETON(RenderSystem)

/// <summary>
/// Initialize the RenderSystem
/// </summary>
void RenderSystem::initialize(json::JSON& document)
{
	std::cout << "RenderSystem initialized" << std::endl;

	if (document.hasKey("RenderSystem"))
	{
		json::JSON render_system = document["RenderSystem"];

		if (render_system.hasKey("Name"))
		{
			name = render_system["Name"].ToString();
		}
		if (render_system.hasKey("width"))
		{
			width = render_system["width"].ToInt();
		}
		if (render_system.hasKey("height"))
		{
			height = render_system["height"].ToInt();
		}
		if (render_system.hasKey("fullscreen"))
		{
			fullscreen = render_system["fullscreen"].ToBool();
		}
	}
}

/// <summary>
/// Updates the RenderSystem
/// </summary>
void RenderSystem::update()
{
	for (auto renderable : renderables)
	{
		renderable->Render();
	}
}

/// <summary>
/// Adds a renderable to the list
/// </summary>
/// <param name="renderable"></param>
void RenderSystem::AddRenderable(IRenderable* renderable)
{
	renderables.push_back(renderable);
}

/// <summary>
/// Removes a renderable from the list
/// </summary>
/// <param name="renderable"></param>
void RenderSystem::RemoveRenderable(IRenderable* renderable)
{
	renderables.remove(renderable);
}
