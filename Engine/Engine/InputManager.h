#pragma once
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

/// <summary>
/// Defines the InputManager class
/// </summary>
class InputManager
{
public:
	void initialize();
	void update();

	DECLARE_SINGLETON(InputManager)
};

#endif //INPUTMANAGER_H