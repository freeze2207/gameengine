#pragma once
#ifndef OBJECT_H
#define OBJECT_H

class Object
{
private:
	bool initialized = false;

	STRCODE id = -1;
	std::string guid;

public:
	Object();
	virtual ~Object() {};

public: 
	virtual void Initialize();
	bool IsInitialized() { return initialized; }
	int GetId() { return id; }
	const std::string& GetGUID() { return guid; }

};

#endif // !OBJECT_H