#pragma once
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <list>
#include "json.hpp"

class Component;

/// <summary>
/// Defines the GameObject class
/// </summary>
class GameObject
{
private:
	std::string name = "";
	std::list<Component*> components;

public:
	GameObject();
	~GameObject();

	void initialize(json::JSON& json_game_object);
	void update();
	void AddComponent(Component* component);
	void RemoveComponent(Component* component);

	const std::string& getName() { return name; }
};

#endif //GAMEOBJECT_H

