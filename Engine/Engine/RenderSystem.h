#pragma once
#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include <string>
#include "json.hpp"

class IRenderable;

/// <summary>
/// Defines the RenderSystem class
/// </summary>
class RenderSystem
{
private:
	std::string name = "";
	int width = 1024;
	int height = 768;
	bool fullscreen = false;

	std::list<IRenderable*> renderables;

public:
	void initialize(json::JSON& document);
	void update();

	void AddRenderable(IRenderable* renderable);
	void RemoveRenderable(IRenderable* renderable);

	DECLARE_SINGLETON(RenderSystem)

};

#endif //OBJECT_H