#include "Core.h"
#include "AssetManager.h"
#include "Asset.h"

IMPLEMENT_SINGLETON(AssetManager)

/// <summary>
/// Initialize the AssetManager
/// </summary>
void AssetManager::initialize()
{
	std::cout << "Asset Manager initialized" << std::endl;
}

/// <summary>
/// Updates the AssetManager
/// </summary>
void AssetManager::update()
{
}

void AssetManager::AddAsset(Asset* asset)
{
	assets.push_back(asset);
}

void AssetManager::RemoveAsset(Asset* asset)
{
	assets.remove(asset);
}
