#pragma once
#ifndef IRENDERABLE_H
#define IRENDERABLE_H

class IRenderable
{
protected:
	IRenderable();
	~IRenderable();

	virtual void Render() = 0;

	friend class RenderSystem;
};

#endif // !IRENDERABLE_H
