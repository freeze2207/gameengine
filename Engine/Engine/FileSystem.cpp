#include "Core.h"
#include "FileSystem.h"
#include <iostream>

IMPLEMENT_SINGLETON(FileSystem)

/// <summary>
/// Initialize the FileSystem
/// </summary>
void FileSystem::initialize()
{
	std::cout << "File System initialized" << std::endl;
}

/// <summary>
/// 
/// </summary>
/// <param name="file_name"></param>
void FileSystem::load(const std::string& file_name)
{
	// Load a file here
}

/// <summary>
/// Updates the FileSystem
/// </summary>
void FileSystem::update()
{
}